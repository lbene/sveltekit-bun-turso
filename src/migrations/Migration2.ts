import type { Kysely } from 'kysely';

export async function up(db: Kysely<never>): Promise<void> {
  await db.schema
    .alterTable('person')
    .addColumn('first_name', 'varchar', (col) => col.notNull())
    .execute()
}

export async function down(db: Kysely<never>): Promise<void> {
  await db.schema.dropTable('person').execute()
}
