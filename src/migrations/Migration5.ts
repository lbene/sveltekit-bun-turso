import type { Kysely } from 'kysely';
import { sql } from 'kysely';

export async function up(db: Kysely<never>): Promise<void> {
  const query = db.schema
    .alterTable('person')
    .addColumn('created_at', 'timestamp', (col) =>
      col.defaultTo(sql`now`).notNull()
    );

  await query
		.execute();
}

export async function down(db: Kysely<never>): Promise<void> {
  await db.schema.dropTable('person').execute()
}
