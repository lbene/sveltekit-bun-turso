import type { Kysely } from 'kysely';

export async function up(db: Kysely<never>): Promise<void> {
  await db.schema
    .createTable('person')
    .addColumn('id', 'serial', (col) => col.primaryKey())
    .execute()
}

export async function down(db: Kysely<never>): Promise<void> {
  await db.schema.dropTable('person').execute()
}
