import type { Kysely } from 'kysely';

export async function up(db: Kysely<never>): Promise<void> {
  await db.schema
    .alterTable('person')
    .addColumn('last_name', 'varchar')
    .execute()
}

export async function down(db: Kysely<never>): Promise<void> {
  await db.schema.dropTable('person').execute()
}
