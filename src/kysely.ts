import { LibsqlDialect } from '@libsql/kysely-libsql';
import { Kysely } from 'kysely';
import type { ColumnType, Generated, Selectable } from 'kysely';
import { env } from '$env/dynamic/private';

export interface Database {
	person: PersonTable;
}

export interface PersonTable {
	id: Generated<number>;
	first_name: string;
	gender: 'man' | 'woman' | 'other';
	last_name: string | null;
	created_at: ColumnType<Date, string | undefined, never>;
}

export type Person = Selectable<PersonTable>;

export const db = new Kysely<Database>({
	dialect: new LibsqlDialect({
		url: env.DB_URL,
		authToken: env.DB_PASS
	}),
});

export async function getPerson(): Promise<Person> {
	return await db
		.selectFrom('person')
		.selectAll()
		.executeTakeFirstOrThrow()
}
