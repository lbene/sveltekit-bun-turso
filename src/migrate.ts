import { env } from '$env/dynamic/private';
import { LibsqlDialect } from '@libsql/kysely-libsql';
import { promises as fs } from 'fs';
import { FileMigrationProvider, Kysely, Migrator } from 'kysely';
import path from 'path';
import type { Database } from './kysely';

async function migrateToLatest() {
  const db = new Kysely<Database>({
    dialect: new LibsqlDialect({
      url: env.DB_URL,
      authToken: env.DB_PASS
    }),
    log: (message) => {
      console.info(message);
    },
  })

  const migrator = new Migrator({
    db,
    provider: new FileMigrationProvider({
      fs,
      path,
      // This needs to be an absolute path.
      migrationFolder: path.join(__dirname, 'migrations'),
    }),
  })

  const { error, results } = await migrator.migrateToLatest()

  results?.forEach((it) => {
    if (it.status === 'Success') {
      console.log(`migration "${it.migrationName}" was executed successfully`)
    } else if (it.status === 'Error') {
      console.error(`failed to execute migration "${it.migrationName}"`)
    }
  })

  if (error) {
    console.error('failed to migrate')
    console.error(error)
    process.exit(1)
  }

  await db.destroy()
}

migrateToLatest()
