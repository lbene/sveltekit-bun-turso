import { getPerson } from '../kysely';

export const load = (async () => {
  return {
    person: await getPerson()
  };
})
